import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  // let imageData = [
  //   {
  //     "path": "flat_1.jpg",
  //     "text": "Flat 1",
  //     "note": "Small"
  //   },
  //   {
  //     "path": "flat_1.jpg",
  //     "text": "Flat 2",
  //     "note": "Small"
  //   },
  //   {
  //     "path": "flat_1.jpg",
  //     "text": "Flat 3",
  //     "note": "Small"
  //   },
  //   {
  //     "path": "flat_1.jpg",
  //     "text": "Flat 4",
  //     "note": "Small"
  //   },
  //   {
  //     "path": "flat_1.jpg",
  //     "text": "Flat 5",
  //     "note": "Small"
  //   },
  //   {
  //     "path": "flat_1.jpg",
  //     "text": "Flat 6",
  //     "note": "Small"
  //   },
  //   {
  //     "path": "flat_medium.jpg",
  //     "text": "Flat 7",
  //     "note": "Medium"
  //   },
  //   {
  //     "path": "flat_medium.jpg",
  //     "text": "Flat 8",
  //     "note": "Medium"
  //   },
  //   {
  //     "path": "flat_medium.jpg",
  //     "text": "Flat 9",
  //     "note": "Medium"
  //   },
  //   {
  //     "path": "flat_medium.jpg",
  //     "text": "Flat 10",
  //     "note": "Medium"
  //   },
  //   {
  //     "path": "flat_medium.jpg",
  //     "text": "Flat 11",
  //     "note": "Medium"
  //   },
  //   {
  //     "path": "flat_medium.jpg",
  //     "text": "Flat 12",
  //     "note": "Medium"
  //   },
  //   {
  //     "path": "flat_large.jpg",
  //     "text": "Flat 13",
  //     "note": "Large"
  //   },
  //   {
  //     "path": "flat_large.jpg",
  //     "text": "Flat 14",
  //     "note": "Large"
  //   },
  //   {
  //     "path": "flat_large.jpg",
  //     "text": "Flat 15",
  //     "note": "Large"
  //   },
  //   {
  //     "path": "flat_large.jpg",
  //     "text": "Flat 16",
  //     "note": "Large"
  //   },
  //   {
  //     "path": "flat_large.jpg",
  //     "text": "Flat 17",
  //     "note": "Large"
  //   },
  //   {
  //     "path": "flat_large.jpg",
  //     "text": "Flat 18",
  //     "note": "Large"
  //   }
  // ];

  // 1v1 arenas
  let imageData = [
    {
      "path": "1v1_8.jpg",
      "text": "1v1 8"
    }
  ];

  let images = imageData.map(e => {
    let note = undefined;
    let text = e["text"].replace("2", "z").replace("5", "s").replace("9", "q");

    if (e["note"]) {
      let smallText = e["note"].replace("2", "z").replace("5", "s");
      note = e.note ? (<div class="small"><div class="shadow">{smallText}</div>,
      <div class="text" style={{backgroundImage: "url(" + e["path"] + ")"}}>{smallText}</div></div>) : null;
    }

    return (
      <div style={{padding: "10em"}}>
        <div id={e["text"].toLowerCase().replace(/ /g, "_")} class="container" style={{backgroundImage: "url(" + e["path"] + ")"}}>
          <div class="shadow" style={{backgroundImage: "url(" + e["path"] + ")"}}>{text}</div>
          <div class="text" style={{backgroundImage: "url(" + e["path"] + ")"}}>{text}</div>
          {
            note
          }
        </div>
      </div>
    );
  });
  
  return (
    <div className="App">
      <header className="App-header" id="header">
        {images}
      </header>
    </div>
  );
}

export default App;
